import React, {useState} from "react";

const Text = function() {

    const [value, setValue] = useState('Текст инпут')

    return (
        <div className="App">
            <h1>{value}</h1>
            <div>
                <input type="text" value={value} onChange={event => setValue(event.target.value)}/>
            </div>
        </div>
    );
}

export default Text;
