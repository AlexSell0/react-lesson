import React, {createRef} from 'react';
import PostItem from "./PostItem";
import {TransitionGroup, CSSTransition} from "react-transition-group";

const PostList = ({posts, title, destroy}) => {
    let newPosts = []
    posts.forEach(postItem => {
        postItem.nodeRef = createRef(null)

        newPosts.push(postItem)
    })

    if (!newPosts.length){
        return (
            <h1 style={{textAlign: "center", margin: '20px auto'}}>Постов не найдено</h1>
        )
    }

    return (
        <div>
            <h1 style={{textAlign: 'center'}}>{title}</h1>

            <TransitionGroup>
            {newPosts.map((post) =>
                <CSSTransition
                    key={post.id}
                    timeout={500}
                    classNames="post"
                    nodeRef={post.nodeRef}
                >
                    <PostItem post={post} destroy={destroy} />
                </CSSTransition>

            )}
            </TransitionGroup>
        </div>
    );
};

export default PostList;