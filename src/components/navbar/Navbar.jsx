import React from 'react';
import {Link} from "react-router-dom";

const Navbar = () => {
    return (
        <div>
            <div className={'navbar'}>
                <div className="navbar__item">
                    <Link to="/">Главная</Link>
                </div>
                <div className="navbar__item">
                    <Link to="/about">About</Link>
                </div>
                <div className="navbar__item">
                    <Link to="/posts">Posts</Link>
                </div>
            </div>
        </div>
    );
};

export default Navbar;