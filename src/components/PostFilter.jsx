import React from 'react';
import MyInput from "./UI/input/MyInput";
import MySelect from "./UI/select/MySelect";

const PostFilter = ({filter, update}) => {

    function sortPosts(key){
        update({sort: key, query: filter.query})
    }


    return (
        <div>
            <MyInput placeholder={'Поиск'}
                     value={filter.query}
                     onInput={event => update({sort: filter.sort, query: event.target.value})}
                     style={{margin: '20px 0'}}
            />

            <h2>Сортировать посты</h2>
            <MySelect
                sortSelect={sortPosts}
                defaultOptions={'Выберите опции'}
                options={[
                    {value: 'title', name: 'По названию'},
                    {value: 'body', name: 'По описанию'}
                ]}
            />

            <br style={{margin: '20px 0'}}/>
        </div>
    );
};

export default PostFilter;