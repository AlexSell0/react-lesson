import MyButton from "./UI/btn/MyButton";

const PostItem = function ({post, destroy}){
    function deletePost(){
        destroy(post.id)
    }

    return (
        <div className="post" ref={post.nodeRef}>
            <div className="post__content">
                <strong>{post.id}. {post.title}</strong>
                <div>
                    {post.body}
                </div>
            </div>

            <div className="post__btns">
                <MyButton cl={'btn-danger'} onClick={deletePost}>Удалить</MyButton>
            </div>
        </div>
    )
}

export default PostItem