import React, {useRef} from 'react';

const MySelect = ({defaultOptions,options,sortSelect}) => {
    const select = useRef()
    function sortSelectValue() {
        let key = select.current.value

        sortSelect(key)
    }

    return (
        <div>
            <select ref={select} name="" id="" onChange={sortSelectValue}>
                <option disabled value="">{defaultOptions}</option>
                {options.map(option => <option key={option.value} value={option.value}>{option.name}</option>)}

            </select>
        </div>
    );
};

export default MySelect;