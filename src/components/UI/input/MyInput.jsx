import React from 'react';

import classes from "./MyInout.module.css";

const MyInput = React.forwardRef((props, ref) => {
    return (
        <div>
            <input ref={ref} type="text" {...props} className={classes}/>
        </div>
    );
});

export default MyInput;