import {getPagesArray} from "../../../utils/page";

export const Pagination = ({totalPage, changePage, page})=> {
    let pagesArray = getPagesArray(totalPage)

    return (
        <div className={'paginations'}>
            {
                pagesArray.map(p =>
                    <span
                        key={p}
                        className={p === page ? 'page page__current' : 'page'}
                        onClick={() => {
                            changePage(p)
                        }}
                    >{p}</span>
                )
            }
        </div>
    )
}