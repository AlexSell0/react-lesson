import React, {createRef, useState} from 'react';
import MyInput from "./UI/input/MyInput";
import MyButton from "./UI/btn/MyButton";

const PostForm = ({length, create}) => {
    const [post, setPost] = useState({title: '', body: ''})

    function addPosts(event){
        event.preventDefault()

        const newPost = {id: length + 1, nodeRef: createRef(null), ...post}

        create(newPost)

        setPost({...post, title:'', body: ''})
    }

    return (
        <div>
            <form action="" onSubmit={addPosts}>
                <MyInput type="text"
                         placeholder={'Название'}
                         value={post.title}
                         onInput={event => setPost({...post, title: event.target.value})}
                />

                <MyInput type="text"
                         placeholder={'Описание'}
                         value={post.body}
                         onInput={event => setPost({...post, body: event.target.value})}
                />

                {/*Неуправляемый инпут*/}
                {/*<MyInput type="text" placeholder={'test'} ref={testInputRef}/>*/}
                <MyButton>Создать пост</MyButton>
            </form>
        </div>
    );
};

export default PostForm;