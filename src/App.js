import React from 'react';
import {BrowserRouter, createBrowserRouter, Link, Route, Routes} from "react-router-dom";
import Home from "./Pages/Home";
import About from "./Pages/About";
import Posts from "./Pages/Posts";
import Navbar from "./components/navbar/Navbar";


function App() {
    const router = createBrowserRouter([
        {
            path: "/",
            element: (<Home/>)
        },
        {
            path: "/about",
            element: (<About/>)
        },
        {
            path: "/posts",
            element: (<Posts/>)
        },
    ])

    return (
        <BrowserRouter>
            <Navbar/>

            <Routes>
                <Route path={'/'} element={<Home/>}></Route>
                <Route path={'/about'} element={<About/>}></Route>
                <Route path={'/posts'} element={<Posts/>}></Route>
            </Routes>

        </BrowserRouter>
    )
}

export default App;
