import {useMemo} from "react";

export const useSortedPost = (posts, sort)=>{
    return useMemo(() => {
        if (sort) {
            return [...posts].sort((a, b) => a[sort].localeCompare(b[sort]))
        }
        return posts
    }, [sort, posts])
}

export const usePosts = (posts, sort, query)=>{
    const sortedPost = useSortedPost(posts, sort)
    const sortedAndSearchPosts = useMemo(()=>{
        if(query){
            return sortedPost(el => el.title.toLowerCase().indexOf(query.toLowerCase()) >=0)
        }

        return sortedPost

    }, [sortedPost,query])

    return sortedAndSearchPosts
}