export const getPageCount = (totalCount, limit) => {
    return Math.ceil(totalCount / limit)
}

export const getPagesArray = (totalCount)=> {
    let result = []

    for (var i = 1; i <= totalCount; i++) {
        result.push(i);
    }

    return result
}