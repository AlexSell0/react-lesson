import React from 'react';

import PostList from "../components/PostList";
import {useEffect, useState} from "react";
import PostForm from "../components/PostForm";
import PostFilter from "../components/PostFilter";
import MyModal from "../components/UI/modal/MyModal";
import MyButton from "../components/UI/btn/MyButton";
import {usePosts} from "../hooks/usePost";
import PostService from "../API/PostService";
import Loader from "../components/UI/loader/Loader";
import {useFetching} from "../hooks/useFetching";
import {getPageCount} from "../utils/page";
import {Pagination} from "../components/UI/pagination/Pagination";

const Posts = () => {
    const [posts, setPosts] = useState([])
    const [totalPage, setTotalPage] = useState(0)

    const [limit, setLimit] = useState(10)
    const [page, setPage] = useState(1)

    const [fetchPosts, isLoading, postError] = useFetching(async ()=>{
        const response = await PostService.getAll(limit, page)
        setPosts(response.data)

        setTotalPage(getPageCount(response.headers['x-total-count'], limit))
    })

    useEffect(() => {
        fetchPosts()
    }, [page]);

    const [filter, setFilter] = useState({sort: '', query: ''})

    function createPost(newPost){
        setPosts([...posts, newPost])
    }

    function deletePost(id){
        let newPosts = posts.filter(el => {
            return el.id !== id
        })

        setPosts(newPosts)
    }

    const sortedAndSearchPosts = usePosts(posts, filter.sort, filter.query)

    const [modal, setModal] = useState(false)

    const changePage = (p)=>{
        setPage(p)
    }

    return (
        <div>
            <MyButton onClick={()=> setModal(true)} >Добавить пост</MyButton>
            <MyModal visible={modal} setVisible={setModal}>
                <PostForm create={createPost} length={Date.now()}></PostForm>
            </MyModal>

            <PostFilter filter={filter} update={setFilter} />

            {
                postError &&
                <h1>Произошла ошибка ${postError}</h1>
            }
            {isLoading ?
                <Loader/> :
                <PostList posts={sortedAndSearchPosts} title={'Список постов 1'} destroy={deletePost}/>
            }

            <Pagination totalPage={totalPage} changePage={changePage} page={page}/>
        </div>
    );
};

export default Posts;